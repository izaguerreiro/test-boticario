FROM python:3.7-slim

WORKDIR /app

COPY . /app

RUN pip install pipenv && \
    pipenv install --system --deploy --ignore-pipfile

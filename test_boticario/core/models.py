from uuid import uuid4

from django.contrib.auth.models import User
from django.db import models

from model_utils import Choices


class BaseModel(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Dealer(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="users")
    name = models.CharField(max_length=255)
    cpf = models.CharField(max_length=11)


class Purchase(BaseModel):
    STATUS = Choices(
        (0, "IN_VALIDATION", "Em validação"),
        (1, "APPROVED", "Aprovada"),
        (1, "DENIED", "Negada"),
    )

    user = models.ForeignKey(Dealer, on_delete=models.CASCADE, related_name="dealers")
    code = models.CharField(max_length=100)
    value = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateField()
    status = models.IntegerField(choices=STATUS, default=STATUS.IN_VALIDATION)
    cashback_percentage = models.PositiveSmallIntegerField()
    cashback_value = models.DecimalField(max_digits=10, decimal_places=2)
